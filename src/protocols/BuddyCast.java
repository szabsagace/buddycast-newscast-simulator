package protocols;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

import output.gephi.GephiBuilder;
import output.sqlite.SqlLiteDbGenerator;
import message.BuddycastMessage;
import message.Message;
import comparator.LastSeenComparator;
import comparator.RecentPeerComparator;
import comparator.SimilarityConnectionComparator;
import simulator.Container;
import simulator.Settings;
import datatype.BootstrapType;
import datatype.CacheMap;
import datatype.ComparatorMap;
import datatype.ConnectionResponse;
import datatype.LimitedQueue;
import datatype.Pair;
import debug.Debug;
import debug.DebugTypes;
import entities.Peer;

/**
 * BuddyCast implementation
 * 
 * @author Szabolcs
 *
 */
public class BuddyCast implements Protocol {

	/*---------------------------------------------------
	   Helper variables
	 ----------------------------------------------------*/
	private Peer hostPeer;

	// for bootstrapping
	private boolean bootstrapped;
	private long lastBootstrapTime;

	private int buddyCastRound;
	private int buddyCastNextInitiate;

	private int randomSeeder = 0;

	// Initialize helper variables
	{
		bootstrapped = false;
		lastBootstrapTime = 0;
		buddyCastRound = 0;
		buddyCastNextInitiate = 0;
	}

	/*---------------------------------------------------
	   BuddyCast Local Database
	 ----------------------------------------------------*/
	private Map<Integer, Pair<Long, Integer>> peers;

	private Map<Integer, Long> connectedTasteBuddies;
	private Map<Integer, Long> connectedRandomPeers;
	private Map<Integer, Long> connectedUnconnectiblePeers;
	private Map<Integer, Long> connectionCandidates;
	private Map<Integer, Long> blockListReceivers;
	private Map<Integer, Long> blockListSenders;

	private LimitedQueue<Integer> myPreferences; // my preferences
	private LimitedQueue<Integer> megaCache; // my torrents
	private CacheMap<Integer, LimitedQueue<Integer>> knownPreferences; // another peers preferences

	private Set<Integer> connectionInitiate; // Connected.
	
	
	/*
	 * Init variables
	 */
	{
		this.peers = new ConcurrentHashMap<Integer, Pair<Long, Integer>>();
		this.connectionCandidates = new ConcurrentHashMap<Integer, Long>();
		this.connectedRandomPeers = new ConcurrentHashMap<Integer, Long>();
		this.connectedTasteBuddies = new ConcurrentHashMap<Integer, Long>();
		this.connectedUnconnectiblePeers = new ConcurrentHashMap<Integer, Long>();
		this.blockListReceivers = new ConcurrentHashMap<Integer, Long>();
		this.blockListSenders = new ConcurrentHashMap<Integer, Long>();
		this.connectionInitiate = new HashSet<Integer>();

		this.myPreferences = new LimitedQueue<Integer>(
				Settings.MAX_MY_PREFERENCES);
		this.megaCache = new LimitedQueue<Integer>(Settings.MAX_MEGACACHE);
		this.knownPreferences = new CacheMap<Integer, LimitedQueue<Integer>>(
				Settings.MAX_KNOWN_PREFERENCES_OF_PEERS);
	}

	/*---------------------------------------------------
	  Constructors
	 ----------------------------------------------------*/
	public BuddyCast() {
	}

	public BuddyCast(Peer hostPeer) {
		this.hostPeer = hostPeer;
	}

	/*------------------------------------------------------
	  Torrent Database Functions
	 -------------------------------------------------------*/

	/**
	 * Manage snapshot of peers' preferences
	 * 
	 * @param peerId Peer who has these preferences
	 * @param preferences  Preferences of peer
	 */
	private synchronized int addPreferences(int peerId, LimitedQueue<Integer> preferences) {
			boolean changed = false;

			if (peerId == this.hostPeer.getPeerId())
				return 0;

			if (this.knownPreferences.containsKey(peerId)) {
				for (Integer torrentId : preferences) {
					if (!this.knownPreferences.get(peerId).contains(torrentId)) {
						this.knownPreferences.get(peerId).add(torrentId);
						this.hostPeer.getStat().discoveredPrefs++;
						changed = true;
					}
					this.hostPeer.getStat().receivedPreferences++;
				}
			} else {
				this.knownPreferences.put(peerId, preferences);
				this.hostPeer.getStat().discoveredPrefs += preferences.size();
				this.hostPeer.getStat().receivedPreferences += preferences.size();
			}

			if (changed)
				this.updateSimilarity(peerId);

			return (changed) ? 1 : 0;
		
	}

	/**
	 * Get peer's preferences
	 * @return preference list
	 */
	private LimitedQueue<Integer> getMyPreferences() {
		return this.myPreferences;
	}

	/**
	 * Add torrent to my preferences
	 * 
	 * @param torrent
	 * @return
	 */
	private synchronized boolean addTorrentToMyPreferences(int torrent) {
		if (this.myPreferences.contains(torrent))
			return true;
		else
			return this.myPreferences.add(torrent);
	}

	/**
	 * Add torrent to peer's local mega cache
	 * 
	 * @param torrent
	 *            Torrent Id, who add to Megacache
	 */
	private synchronized boolean addTorrentToMegaCache(int torrent) {
		if (this.megaCache.contains(torrent)) {
			return true;
		}

		this.hostPeer.getStat().discoveredTorrents++;
		return this.megaCache.add(torrent);
	}
	
	/**
	 * Check torrent is new or not.
	 * @param torrentId 
	 */
	private synchronized boolean isNewTorrent(int torrentId) {
		if (Container.torrentsList.contains(torrentId))
			return false;

		this.hostPeer.getStat().newTorrentImported++;

		Container.torrentsList.add(torrentId);
		Debug.debug(hostPeer.getTime(), DebugTypes.INFO,
				hostPeer.getPeerId() + ": INJECTED NEW TORRENT (" + torrentId
						+ ")");
		return true;
	}

	/**
	 * Remove connection candidate
	 * @param peer
	 */
	private void removeConnectionCandidate(int peer) {
		this.connectionCandidates.remove(peer);
	}
	
	

	
	/**
	 * Update Senders Block List
	 */
	private synchronized void updateSendBlockList() {
		for (Integer peerId : this.blockListReceivers.keySet()) {
			if (this.hostPeer.getTime() >= this.blockListReceivers.get(peerId))
				this.blockListReceivers.remove(peerId);
		}

		for (Integer peerId : this.blockListSenders.keySet()) {
			if (this.hostPeer.getTime() >= this.blockListSenders.get(peerId))
				this.blockListSenders.remove(peerId);
		}

	}

	/**
	 * Update Similarity 
	 * @param peerId peer who wants to update similarity
	 */
	private synchronized void updateSimilarity(int peerId) {
		if (this.peers.containsKey(peerId))
			this.peers.get(peerId).setSecond(
					this.hostPeer.getSimilarity().getSimilarity(peerId,
							myPreferences, knownPreferences));
	}

	
	/**
	 * Update all similarity, who know preferences
	 * @param torrentId
	 */
	private synchronized void updateAllSimilarity(int torrentId) {
		for (Entry<Integer, LimitedQueue<Integer>> e : this.knownPreferences
				.entrySet()) {
			if (e.getValue().contains(torrentId)) {
				this.updateSimilarity(e.getKey());
				break;
			}
		}
	}

	
	
	
	/*---------------------------------------------------
	  Search Actions and Methods
	 ----------------------------------------------------*/
	
	
	/**
	 * Protocol call to start a search
	 */
	public synchronized void searchAction(int torrentId, boolean fillUpNetwork) {

		// this fill up my network
		if (fillUpNetwork) {
			if (this.isNewTorrent(torrentId))
				this.addTorrentToMegaCache(torrentId);

			this.addTorrentToMyPreferences(torrentId);

			return;
		}

		this.hostPeer.getStat().startedSearch++;

		// if not fill network, we start really search
		if (this.searchTorrent(torrentId, 0)) {
			this.hostPeer.getStat().hittedSearch++;
			this.addTorrentToMegaCache(torrentId);
			this.addTorrentToMyPreferences(torrentId);
			this.updateAllSimilarity(torrentId);
		} else if (this.isNewTorrent(torrentId)) {
			this.addTorrentToMyPreferences(torrentId);
			this.addTorrentToMegaCache(torrentId);
		} else {
			this.addTorrentToMyPreferences(torrentId);
		}
	}

	/**
	 * Search method 
	 */
	public boolean searchTorrent(int torrentId, int circular) {
		if (circular == 0)
			Debug.debug(hostPeer.getTime(), DebugTypes.INFO,
					hostPeer.getPeerId() + ": START SEARCH (" + torrentId + ")");

		// First we check my local database
		if (this.searchLocalMegaCache(torrentId, false)) {
			Debug.debug(hostPeer.getTime(), DebugTypes.INFO,
					hostPeer.getPeerId() + ": SUCCESS SEARCH(" + torrentId
							+ ") - MY LOCAL CACHE");
			return true;
		}

		// Stop conditional of recursive torrent searching (we are able to create fluid search)
		if (circular == Settings.SEARCH_CIRCULAR)
			return false;

		// Next Taste Buddies Search
		for (Integer tasteBuddiesPeer : this.connectedTasteBuddies.keySet()) {
			if (Container.allPeers.get(tasteBuddiesPeer).getProtocol()
					.searchTorrent(torrentId, circular + 1)) {
				if (circular == 0)
					Debug.debug(hostPeer.getTime(), DebugTypes.INFO, hostPeer.getPeerId() + ": SUCCESS SEARCH(" + torrentId + " from: " + tasteBuddiesPeer + ")");
				return true;
			}
		}

		// Next Random Peer Search Buddies Search
		for (Integer randomPeer : this.connectedRandomPeers.keySet()) {
			if (Container.allPeers.get(randomPeer).getProtocol()
					.searchTorrent(torrentId, circular + 1)) {
				
				if (circular == 0)
					Debug.debug(hostPeer.getTime(), DebugTypes.INFO, hostPeer.getPeerId() + ": SUCCESS SEARCH(" + torrentId + " from: " + randomPeer + ")");
				return true;
			}
		}

		if (circular == 0)
			Debug.debug(hostPeer.getTime(), DebugTypes.INFO,
					hostPeer.getPeerId() + ": FAILED SEARCH(" + torrentId + ")");

		return false;
	}

	/**
	 * Check LocalMegaCache, and if we find the torrent, we go back with true.
	 * 
	 * @param torrentId
	 * @return
	 */
	private boolean searchLocalMegaCache(int torrentId, boolean enableCheckMyPref) {
		if (enableCheckMyPref && this.myPreferences.contains(torrentId))
			return true;

		return this.megaCache.contains(torrentId);
	}

	/*---------------------------------------------------
	  Buddycast Implementation
	 ----------------------------------------------------*/

	/**
	 * Start working buddycast 
	 */
	public void action() {
		int targetPeer;

		this.updateSendBlockList();

		this.hostPeer.getStat().startedBuddycastAction++;

		if (this.buddyCastRound % Settings.ROUND_CHECK_CONNECTION == 0)
			this.hostPeer.checkConnections(true);

		if (this.buddyCastNextInitiate == 0) {
			targetPeer = this.selectTarget();
			if (targetPeer != -1000) {
				this.startBuddyCastAction(targetPeer);
			}// send buddycast

		} else
			buddyCastNextInitiate--;

		buddyCastRound++;

		if (buddyCastRound % Settings.LOCAL_REPORT_ROUND == 0)
			this.doRoundActions();

		this.hostPeer.getStat().connectableBuddycastRounds += this.hostPeer
				.isConnectable() ? 1 : 0;
	}

	
	/**
	 * Start a buddycast action
	 * @param targetPeer
	 * @return
	 */
	private ConnectionResponse startBuddyCastAction(int targetPeer) {
		ConnectionResponse response = this.hostPeer.connectPeer(targetPeer);
		//k�ne ide isblocked!!!
		
		this.removeConnectionCandidate(targetPeer);
		this.blockPeer(targetPeer, this.blockListSenders);

		if (response == ConnectionResponse.OK) {
			Debug.debug(hostPeer.getTime(), DebugTypes.INFO,
					hostPeer.getPeerId() + ": SEND BUDDYCAST(target: "
							+ targetPeer + ")");
			BuddycastMessage message = this.createMessage(targetPeer);
			message.reply = true;

			this.hostPeer.sendMessage(targetPeer, message);
			this.connectionInitiate.add(targetPeer);
		}

		return response;
	}

	/**
	 * Create Buddycast Message
	 * @param targetPeer
	 * @return
	 */
	private BuddycastMessage createMessage(int targetPeer) {
		BuddycastMessage msg = new BuddycastMessage();

		msg.senderId = this.hostPeer.getPeerId();
		msg.isConnectable = this.hostPeer.isConnectable();

		msg.myPreferences = this.getMyPreferences();
		msg.tasteBuddies = this.getTasteBuddiesForMessage(targetPeer);
		msg.randomPeers = this.getRandomPeersForMessage(targetPeer);

		return msg;
	}
	

	/**
	 * Handle got message (protocol call)
	 * @param Message message
	 */
	public void handleMessage(Message message) {
		Debug.debug(hostPeer.getTime(), DebugTypes.INFO,
				hostPeer.getPeerId() + ": GOT MESSAGE (from: "
						+ message.senderId + ")");

		if (this.isBlocked(message.senderId, this.blockListReceivers))
			return;

		this.handleBuddyCastMsg((BuddycastMessage) message);
		this.hostPeer.getStat().receivedPeers += ((BuddycastMessage) message).tasteBuddies
				.size() + ((BuddycastMessage) message).randomPeers.size();

		if (!this.bootstrapped) {
			if (Settings.BOOTSTRAP_TYPE == BootstrapType.ONLY_ONCE)
				this.bootstrapped = true;
			else if (Settings.BOOTSTRAP_TYPE == BootstrapType.BOOTSTRAP_INTERVAL) {
				this.bootstrapped = true;
				this.lastBootstrapTime = this.hostPeer.getTime();
			}
		}

		if (message.reply) {
			this.hostPeer.getStat().answeredMessages++;
			if (!this.isBlocked(message.senderId, this.blockListSenders)) {
				this.replyBuddycast(message.senderId);
				this.buddyCastNextInitiate++;
			}
		}

		this.blockPeer(message.senderId, this.blockListReceivers);
	}

	/**
	 * Buddycast specified handling
	 * @param message
	 */
	private void handleBuddyCastMsg(BuddycastMessage message) {

		this.addPreferences(message.senderId, message.myPreferences);

		// TASTE BUDDIES -
		for (Entry<Integer, Pair<LimitedQueue<Integer>, Long>> p : message.tasteBuddies
				.entrySet()) {
			if (this.addPeer(p.getKey()) == 1)
				this.updateLastSeen(p.getKey(), p.getValue().getSecond());
			// ?????????????????
			this.addPreferences(p.getKey(), p.getValue().getFirst());
			this.addConnectionCandidate(p.getKey(), p.getValue().getSecond());
		}

		// RANDOM PEERS
		for (Entry<Integer, Long> p : message.randomPeers.entrySet()) {
			if (this.addPeer(p.getKey()) == 1)
				this.updateLastSeen(p.getKey(), p.getValue());

			this.addConnectionCandidate(p.getKey(), p.getValue());
		}

		this.addPeerToConnectionLists(message.senderId, message.isConnectable);

	}

	/**
	 * Reply buddycast message
	 * @param targetPeer
	 */
	private void replyBuddycast(int targetPeer) {
		Debug.debug(hostPeer.getTime(), DebugTypes.INFO,
				hostPeer.getPeerId() + ": REPLY BUDDYCAST(" + targetPeer + ")");

		BuddycastMessage message = this.createMessage(targetPeer);
		message.reply = false;
		this.hostPeer.sendMessage(targetPeer, message);

		this.removeConnectionCandidate(targetPeer);
		this.blockPeer(targetPeer, blockListSenders);
	}
	
	
	/**
	 * Create Taste Buddies list for buddycast message
	 * @param targetPeer target peer
	 * @return
	 */
	private Map<Integer, Pair<LimitedQueue<Integer>, Long>> getTasteBuddiesForMessage(int targetPeer) {
		Map<Integer, Pair<LimitedQueue<Integer>, Long>> selectedPeers = new HashMap<Integer, Pair<LimitedQueue<Integer>, Long>>();

		LimitedQueue<Integer> peers = this
				.selectRandomList(
						(Settings.MAX_CONNECTION_TASTE_BUDDIES >= Settings.MESSAGE_TASTE_BUDDIES_NUMBER) ? Settings.MESSAGE_TASTE_BUDDIES_NUMBER
								: Settings.MAX_CONNECTION_TASTE_BUDDIES,
						this.connectedTasteBuddies.keySet());

		synchronized(knownPreferences) {
			for (Integer peerId : peers) {
				if (peerId == targetPeer
						|| this.knownPreferences.get(peerId) == null)
					continue;
	
				Pair<LimitedQueue<Integer>, Long> p = new Pair<LimitedQueue<Integer>, Long>(
						this.selectRandomList(
								Settings.MAX_KNOWN_PREFERENCES_OF_PEERS,
								this.knownPreferences.get(peerId)),
						this.hostPeer.getTime());
	
				selectedPeers.put(peerId, p);
			}
		}

		return selectedPeers;
	}

	/**
	 * Create Random Peers list for message
	 * @param targetPeer
	 * @return
	 */
	private Map<Integer, Long> getRandomPeersForMessage(int targetPeer) {
		Map<Integer, Long> selectedPeers = new HashMap<Integer, Long>();

		Queue<Integer> peers = this
				.selectRandomList(
						(Settings.MAX_CONNECTION_RANDOM_PEERS >= Settings.MESSAGE_RANDOM_PEERS_NUMBER) ? Settings.MESSAGE_RANDOM_PEERS_NUMBER
								: Settings.MAX_CONNECTION_RANDOM_PEERS,
						this.connectedRandomPeers.keySet());

		for (Integer peerId : peers) {
			if (peerId == targetPeer)
				continue;
			selectedPeers.put(peerId, this.hostPeer.getTime());
		}

		return selectedPeers;
	}

	/**
	 * Helper method for random peers list generation
	 * @param numberInList
	 * @param list
	 * @return
	 */
	private synchronized LimitedQueue<Integer> selectRandomList(int numberInList,
			Collection<Integer> list) {
		LimitedQueue<Integer> selectedList = new LimitedQueue<Integer>(-1);

		// First if the list is empty, we return empty vector
		if(list == null)
			return selectedList;
		
		if (list.size() == 0)
			return selectedList;

		int numGenerating = (numberInList > list.size()) ? list.size()
				: numberInList;

		Random rnd = new Random();
		rnd.setSeed(System.currentTimeMillis() * this.randomSeeder++);

		// Randomize Unique Numbers
		int generatedValue = 0;
		Vector<Integer> uniqueNumbers = new Vector<Integer>();

		while (uniqueNumbers.size() < numGenerating) {
			generatedValue = rnd.nextInt(list.size()) + 1;

			if (uniqueNumbers.contains(generatedValue))
				continue;
			uniqueNumbers.add(generatedValue);
		}

		// Iterator
		for (Integer id : uniqueNumbers) {
			Iterator<Integer> it = list.iterator();
			int index = 1;
			while (it.hasNext()) {
				if (index++ == id) {
					selectedList.add(it.next());
					break;
				}
			}
		}

		return selectedList;
	}



	/*---------------------------------------------------
	   BuddyCast Specified Administration methods
	 ----------------------------------------------------*/

	/**
	 * Add peer to peers map
	 * @param peerId
	 * @return
	 */
	private int addPeer(int peerId) {
		if (this.hostPeer.getPeerId() == peerId)
			return -1; // nem tudod magadat hozzáadni

		synchronized(peers) {
			if (!peers.containsKey(peerId)) {
				peers.put(peerId, new Pair<Long, Integer>(0l, 0));
				this.hostPeer.getStat().discoveredPeers++;
				return 1;
			}
		}

		return 0;
	}

	/**
	 * update last seen in peers map
	 * @param peerId
	 * @param lastSeenTime
	 */
	private synchronized void updateLastSeen(int peerId, long lastSeenTime) {
		if (peers.containsKey(peerId))
			peers.get(peerId).setFirst(lastSeenTime);
	}

	/**
	 * Block peer to blocklist
	 * @param blockingPeerId peer id
	 * @param blockList list who wants to put
	 */
	private void blockPeer(int blockingPeerId, Map<Integer, Long> blockList) {
		blockList.put(blockingPeerId, this.hostPeer.getTime()
				+ Settings.BLOCKING_INTERVAL);
	}

	/**
	 * Add peer to connection candidate
	 * @param peer
	 * @param lastSeen
	 */
	public synchronized void addConnectionCandidate(int peer, long lastSeen) {
		if (this.isBlocked(peer, this.blockListSenders))
			return;

		if (this.connectionCandidates.containsKey(peer)
				|| this.connectionCandidates.size() < Settings.MAX_CONNECTION_CANDIDATES) {
			this.connectionCandidates.put(peer, lastSeen);
			return;
		}

		// ComparatorMap<Integer, Long>

		ComparatorMap<Integer, Long> sortedMap = new ComparatorMap<Integer, Long>(
				Collections.reverseOrder(new LastSeenComparator()));
		sortedMap.putAll(this.connectionCandidates);

		this.removeConnectionCandidate(sortedMap.lastKey());
		// if(sortedMap.firstEntry().getValue() < lastSeen) // I put this,
		// because no delete with older items
		// this.removeConnectionCandidate((Integer)sortedMap.keySet().toArray()[0]);

	}


	/**
	 * Add peer to connection list
	 * @param peer
	 * @param connectable
	 */
	private void addPeerToConnectionLists(int peer, boolean connectable) {

		if (this.connectedTasteBuddies.containsKey(peer))
			this.connectedTasteBuddies.remove(peer);
		else if (this.connectedRandomPeers.containsKey(peer))
			this.connectedRandomPeers.remove(peer);
		else if (this.connectedUnconnectiblePeers.containsKey(peer))
			this.connectedUnconnectiblePeers.remove(peer);

		long connectionTime = this.hostPeer.getTime();

		if (connectable) {
			if (!addPeerToTasteBuddies(peer, connectionTime))
				addPeerToRandomPeers(peer, connectionTime);
		} else
			addPeerToUnconnectablePeers(peer, connectionTime);

	}

	/**
	 * Add peer to taste buddies
	 * @param peer
	 * @param connectionTime
	 * @return
	 */
	private synchronized boolean addPeerToTasteBuddies(int peer, long connectionTime) {

		// if peer is in the list, we shouldn't add it, just return true.
		if (this.connectedTasteBuddies.containsKey(peer))
			return true;

		int similarity = this.peers.get(peer).getSecond();

		if (similarity > 0) {
			if (this.connectedTasteBuddies.size() < Settings.MAX_CONNECTION_TASTE_BUDDIES) {
				this.connectedTasteBuddies.put(peer, connectionTime);
				return true;
			} else {
				ComparatorMap<Integer, Pair<Long, Integer>> similarMap = new ComparatorMap<Integer, Pair<Long, Integer>>(
						new SimilarityConnectionComparator());
				
				for (Integer peerId : this.connectedTasteBuddies.keySet())
					similarMap.put(peerId, new Pair<Long, Integer>(
							this.connectedTasteBuddies.get(peerId), this.peers
									.get(peerId).getSecond()));

				int oldestElement = similarMap.lastKey();
				int oldestSimilarity = similarMap.lastValue().getSecond();

				if (similarity > oldestSimilarity) {
					if (this.connectedTasteBuddies.remove(oldestElement) != null) {
						this.addPeerToRandomPeers(oldestElement, similarMap
								.get(oldestElement).getFirst());
					}
					this.connectedTasteBuddies.put(peer, connectionTime);
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Add peer to random peer list
	 * @param peer
	 * @param connectionTime
	 */
	public synchronized void addPeerToRandomPeers(int peer, long connectionTime) {
		if (!connectedRandomPeers.containsKey(peer)) {
			int out = addNewPeerToConnList(peer, connectionTime,
					Settings.MAX_CONNECTION_RANDOM_PEERS,
					this.connectedRandomPeers);
			if (out != -100)
				this.hostPeer.closeConnection(out);
		}
	}

	/**
	 * Add peer to unconnectable peers
	 * @param peer
	 * @param connectionTime
	 */
	private synchronized void addPeerToUnconnectablePeers(int peer, long connectionTime) {
		if (!connectedUnconnectiblePeers.containsKey(peer)) {
			int out = addNewPeerToConnList(peer, connectionTime,
					Settings.MAX_CONNECTION_UNCONNECTABLE_PEERS,
					this.connectedUnconnectiblePeers);
			if (out != -100)
				this.hostPeer.closeConnection(out);
		}
	}

	/**
	 * Add peer to list
	 * @param peer
	 * @param connectionTime
	 * @param max
	 * @param connectedList
	 * @return
	 */
	private int addNewPeerToConnList(int peer, long connectionTime, int max,
			Map<Integer, Long> connectedList) {
		if (connectedList.size() < max) {
			connectedList.put(peer, connectionTime);
			return -100;
		} else {
			ComparatorMap<Integer, Long> sortedMap = new ComparatorMap<Integer, Long>(
					new LastSeenComparator());
			sortedMap.putAll(connectedList);

			long oldestConnectionTime = sortedMap.lastValue();

			if (connectionTime > oldestConnectionTime) {
				connectedList.remove(sortedMap.lastKey());
				connectedList.put(peer, connectionTime);
				return sortedMap.lastKey();
			}
		}
		return peer;
	}

	
	/**
	 * Select target peer for buddycast message send
	 * @return
	 */
	private int selectTarget() {
		int target = -1000;

		if (this.hostPeer.isSuperPeer())
			return target;
		else {
			if (connectionCandidates.isEmpty())
				bootstrap(Settings.BOOTSTRAP_NUM);

			if (connectionCandidates.isEmpty())
				return target;

			Random generator = new Random();
			generator.setSeed(System.currentTimeMillis() * randomSeeder++);
			int r = generator.nextInt(10);

			if (r < Settings.ALPHA * 10)
				target = this.selectTargetTasteBuddies();
			else
				target = this.selectTargetRandomPeers();

			return target;
		}
	}

	/**
	 * Select the most similar peer
	 * @return
	 */
	private synchronized int selectTargetTasteBuddies() {
		int maxSimilarity = -1;
		int maxPeerId = -100;
		for (Integer peerId : this.connectionCandidates.keySet()) {
			int similarity = this.peers.get(peerId).getSecond();
			if (similarity > maxSimilarity) {
				maxSimilarity = similarity;
				maxPeerId = peerId;
			}
		}
		return maxPeerId;
	}

	/**
	 * Select a random peer in connection candidates list
	 * @return peer
	 */
	private synchronized int selectTargetRandomPeers() {
		Random generator = new Random();
		generator.setSeed(System.currentTimeMillis() * randomSeeder++);
		int randomPeer = generator.nextInt(this.connectionCandidates.size());
		return (Integer) this.connectionCandidates.keySet().toArray()[randomPeer];
	}

	
	/*---------------------------------------------------
	   Protocol Interface Methods
	 ----------------------------------------------------*/

	/**
	 * When the peer connect firstly, it can connect superpeers (because it
	 * knows their addresses)
	 */
	public void firstBootstrap() {
		for (Peer p : Container.superPeers) {
			if (p.getPeerId() != hostPeer.getPeerId()) {
				this.addPeer(p.getPeerId());
				this.updateLastSeen(p.getPeerId(), hostPeer.getTime());
			}
		}
	}

	/**
	 * General bootstrap
	 * 
	 * @param numberOfRecentPeers
	 *            Number of Recent Peers, who wants to initialize
	 */
	public void bootstrap(int numberOfRecentPeers) {
		// If the BOOTSTRAP_TYPE is setted interval bootstrapping, we check
		// this.
		Debug.debug(hostPeer.getTime(), DebugTypes.INFO,
				hostPeer.getPeerId() + ": BOOTSTRAPPING");

		if (Settings.BOOTSTRAP_TYPE == BootstrapType.BOOTSTRAP_INTERVAL
				&& bootstrapped) {
			if (this.hostPeer.getTime() - lastBootstrapTime >= Settings.BOOTSTRAP_INTERVAL)
				bootstrapped = false;
		}

		// If bootstrapped, we exit this method
		if (bootstrapped)
			return;

		// we sort peers by recently
		ComparatorMap<Integer, Pair<Long, Integer>> sortedPeers = new ComparatorMap<Integer, Pair<Long, Integer>>(
				new RecentPeerComparator());
		sortedPeers.putAll(this.peers);

		int i = 0;
		
		// iterate a sortedPeers and if peer is not in blocklist senders, it will be added to connection candidate.
		for (Map.Entry<Integer, Pair<Long, Integer>> entry : sortedPeers
				.entryList()) {
			if (i == numberOfRecentPeers)
				break;

			int peerId = entry.getKey();
			if (!isBlocked(peerId, blockListSenders)) {
				i++;
				this.addConnectionCandidate(peerId, this.peers.get(peerId)
						.getFirst());
			}
		}

		this.bootstrapped = true;
	}

	/**
	 * When added new connection, we call this method of protocol
	 */
	public void addedConnection(int peerId) {
		if (Settings.OUTPUT_GEPHY)
			GephiBuilder.addEdge(hostPeer.getPeerId(), peerId,
					hostPeer.getTime());
		this.addPeer(peerId);
		this.updateLastSeen(peerId, this.hostPeer.getTime());
	}

	/**
	 * Resetting values when peers go offline.
	 */
	public void resetValues() {
		// reset values
		this.blockListReceivers.clear();
		this.blockListSenders.clear();
		this.connectionCandidates.clear();
		this.connectionInitiate.clear();
		this.connectedRandomPeers.clear();
		this.connectedTasteBuddies.clear();
		this.connectedUnconnectiblePeers.clear();

		// bootstrapping values
		this.bootstrapped = false;
		this.lastBootstrapTime = 0;
	}

	
	/**
	 * Check keep alive
	 */
	public boolean checkKeepAlive(int peerId) {
		if (this.connectedRandomPeers.containsKey(peerId)
				|| this.connectedTasteBuddies.containsKey(peerId)
				|| this.connectionInitiate.contains(peerId))
			return true;
		else
			return false;
	}

	/**
	 * Check that peer is in the blocklist.
	 */
	public synchronized boolean isBlocked(int peer, Map<Integer, Long> blockList) {
		if (!blockList.containsKey(peer))
			return false;

		if (blockList.containsKey(peer)
				&& hostPeer.getTime() >= blockList.get(peer)) {
			blockList.remove(peer);
			return false;
		}

		return true;
	}


	public Map<Integer, Long> getReceiversBlockList() {
		return this.blockListReceivers;
	}

	public Map<Integer, Long> getSendersBlockList() {
		return this.blockListSenders;
	}

	public void setHostPeer(Peer p) {
		this.hostPeer = p;
	}

	public Set<Integer> getConnectedTasteBuddies() {
		return this.connectedTasteBuddies.keySet();
	}

	public Set<Integer> getConnectedRandomPeers() {
		return this.connectedRandomPeers.keySet();
	}
	
	
	/*-------------------------------------
	 * Statistical methods and round actions
	 * -------------------------------------
	 */
	
	/**
	 * Round actions
	 */
	private void doRoundActions() {
		Debug.debug(hostPeer.getTime(), DebugTypes.STAT,
				hostPeer.getPeerId() + ": GENERATE LOCAL ROUND REPORT TO DB");
		this.maintenancePeerDb();
		this.reportPeerInfo();
	}

	/**
	 * call system garbage collector
	 */
	private void maintenancePeerDb() {
		System.gc();
	}

	private void reportPeerInfo() {
		this.updateStatisticData();

		// Save information to Db
		if (Settings.OUTPUT_DB)
			SqlLiteDbGenerator.addItem("local_report", hostPeer.getTime(),
					this.hostPeer.getPeerId(),
					this.hostPeer.getStat().startedBuddycastAction,
					this.hostPeer.getStat().connectableBuddycastRounds,
					this.hostPeer.getStat().discoveredPeers,
					this.hostPeer.getStat().discoveredTorrents,
					this.hostPeer.getStat().discoveredPrefs,
					this.hostPeer.getStat().receivedPeers,
					this.hostPeer.getStat().receivedPreferences,
					this.hostPeer.getStat().sentMessages,
					this.hostPeer.getStat().receivedMessages,
					this.hostPeer.getStat().answeredMessages,
					this.hostPeer.getStat().openedConnections,
					this.hostPeer.getStat().closedConnections,
					this.hostPeer.getStat().startedSearch,
					this.hostPeer.getStat().hittedSearch,
					this.hostPeer.getStat().newTorrentImported,
					this.hostPeer.getStat().connectedTasteBuddiesNum,
					this.hostPeer.getStat().connectedRandomPeersNum,
					this.hostPeer.getStat().sendBlockListNum,
					this.hostPeer.getStat().receivedBlockListNum,
					this.hostPeer.getStat().averageSimilarityTasteBuddies,
					this.hostPeer.getStat().averageSimilarityRandomPeers);
	}
	
	public void updateStatisticData() {
		this.hostPeer.getStat().connectedRandomPeersNum = this.connectedRandomPeers
				.size();
		this.hostPeer.getStat().connectedTasteBuddiesNum = this.connectedTasteBuddies
				.size();
		this.hostPeer.getStat().sendBlockListNum = this.blockListSenders.size();
		this.hostPeer.getStat().receivedBlockListNum = this.blockListReceivers
				.size();

		this.hostPeer.getStat().peersArrayNum = this.peers.size();
		this.hostPeer.getStat().averageSimilarityRandomPeers = this
				.getSimilarityStat(this.connectedRandomPeers).getMean();
		this.hostPeer.getStat().averageSimilarityTasteBuddies = this
				.getSimilarityStat(this.connectedTasteBuddies).getMean();
	}

	private synchronized SummaryStatistics getSimilarityStat(Map<Integer, Long> list) {
		SummaryStatistics sum = new SummaryStatistics();
			for (Integer item : list.keySet()) {
				if (this.peers.get(item) != null) {
					sum.addValue(this.peers.get(item).getSecond());
				}
			}
		

		return sum;
	}


}
