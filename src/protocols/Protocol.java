package protocols;

import java.util.Map;
import java.util.Set;

import entities.Peer;
import message.Message;

public interface Protocol {
	public void addedConnection(int peerId);
	public void resetValues();
	public boolean checkKeepAlive(int peerId);
	public void firstBootstrap();
	public void bootstrap(int numberOfRecentPeers);
	public boolean isBlocked(int peer, Map<Integer, Long> blockList);
	public void searchAction(int torrentId, boolean fillUpNetwork);
	public boolean searchTorrent(int torrentId, int circular);
	public void handleMessage(Message message);
	public void setHostPeer(Peer p);
	public void action();
	public void updateStatisticData();
	public Set<Integer> getConnectedTasteBuddies();
	public Set<Integer> getConnectedRandomPeers();
}
