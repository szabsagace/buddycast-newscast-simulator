package output;


public class Statistic {

	public int startedBuddycastAction = 0;
	public int connectableBuddycastRounds = 0;
	public int discoveredPeers = 0; // new peers added to peers array
	public int discoveredTorrents = 0; // local cache
	public int discoveredPrefs = 0; // manage preferences knownPreferences (new for me)
	public int receivedPeers = 0; // get peers from message
	public int receivedPreferences = 0; // all preferences, what get from message
	public int sentMessages = 0; 
	public int receivedMessages = 0;
	public int answeredMessages = 0;

	public int openedConnections = 0;
	public int closedConnections = 0;
	public int startedSearch = 0;
	public int hittedSearch = 0;
	public int newTorrentImported = 0;
	
	public int connectedTasteBuddiesNum = 0;
	public int connectedRandomPeersNum = 0;
	public int sendBlockListNum = 0;
	public int receivedBlockListNum = 0;
	
	public double averageSimilarityTasteBuddies = 0;
	public double averageSimilarityRandomPeers = 0;
	
	public int peersArrayNum = 0;
	
	
	public Statistic() {
		startedSearch = 0;
	}


	@Override
	public String toString() {
		return "Statistic [startedBuddycastAction=" + startedBuddycastAction
				+ ", connectableBuddycastRounds=" + connectableBuddycastRounds
				+ ", discoveredPeers=" + discoveredPeers
				+ ", discoveredTorrents=" + discoveredTorrents
				+ ", discoveredPrefs=" + discoveredPrefs + ", receivedPeers="
				+ receivedPeers + ", receivedPreferences="
				+ receivedPreferences + ", sentMessages=" + sentMessages
				+ ", receivedMessages=" + receivedMessages
				+ ", answeredMessages=" + answeredMessages
				+ ", openedConnections=" + openedConnections
				+ ", closedConnections=" + closedConnections
				+ ", startedSearch=" + startedSearch + ", hittedSearch="
				+ hittedSearch + ", newTorrentImported=" + newTorrentImported
				+ ", connectedTasteBuddiesNum=" + connectedTasteBuddiesNum
				+ ", connectedRandomPeersNum=" + connectedRandomPeersNum
				+ ", sendBlockListNum=" + sendBlockListNum
				+ ", receivedBlockListNum=" + receivedBlockListNum
				+ ", averageSimilarityTasteBuddies="
				+ averageSimilarityTasteBuddies
				+ ", averageSimilarityRandomPeers="
				+ averageSimilarityRandomPeers + "]";
	}


	

}
