package simulator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import entities.Peer;


public class Container {
	public static Vector<Peer> allPeers;
	public static List<Peer> superPeers;
	public static String fileName;
	public static long SIM_BEGIN = Long.MAX_VALUE;
	public static long SIM_END = Long.MIN_VALUE;
	public static Set<Integer> torrentsList; // this is help for faster search that is new torrent or not. 
	public static int events_num = 0;
	static {
		allPeers = new Vector<Peer>();
		superPeers = new ArrayList<Peer>();
		fileName = Settings.TRACE_FILE;
		torrentsList = new HashSet<Integer>();
	}
}
