package simulator;

import datatype.BootstrapType;

public class Settings {
	public static long TIMEOUT = 5 * 60;
	public static int BOOTSTRAP_NUM = 5;

	public static BootstrapType BOOTSTRAP_TYPE = BootstrapType.BOOTSTRAP_INTERVAL;
	public static long BOOTSTRAP_INTERVAL = 5 * 60;
	public static int NUMBER_OF_SUPERPEERS = 10;
	public static long BLOCKING_INTERVAL = 5 * 60;

	public static int MAX_CONNECTION_CANDIDATES = 10;
	public static int MAX_CONNECTION_TASTE_BUDDIES = 10;
	public static int MAX_CONNECTION_RANDOM_PEERS = 10;
	public static int MAX_CONNECTION_UNCONNECTABLE_PEERS = 10;

	public static int MESSAGE_TASTE_BUDDIES_NUMBER = 20;
	public static int MESSAGE_RANDOM_PEERS_NUMBER = 5;
	public static int MESSAGE_PEER_PREFERENCES = 5;

	public static int MAX_KNOWN_PREFERENCES_OF_PEERS = 10;
	public static int MAX_MY_PREFERENCES = 10;
	public static int MAX_MEGACACHE = 3;

	public static int ROUND_CHECK_CONNECTION = 5;

	public static double ALPHA = 0.6;

	public static boolean LOG_TO_FILE = false;
	public static boolean TRACE_TO_SCREEN = false;

	public static String TRACE_FILE = "E:\\connbtuser100.log";
	public static String OUTPUT_LOG_FOLDER = "log/";
	public static boolean OUTPUT_GEPHY = true;
	public static String OUTPUT_GEPHY_FOLDER = "gephy/";
	public static boolean OUTPUT_DB = true;
	public static String OUTPUT_DB_FOLDER = "db/";

	public static long FILL_UP_NETWORK_TIME = 172800;
	public static long LOCAL_REPORT_ROUND = 10;
	public static long GLOBAL_REPORT_ROUND = 1000;

	public static long BUDDYCAST_INTERVAL = 5 * 60 * 60;

	public static boolean WATCH_FIREWALL = true;
	public static int SEARCH_CIRCULAR = 2;
	public static boolean ALWAYS_ONLINE = true;
	public static String SIMILARITY_TYPE = "RANDOM";

	public Settings() {
	}

	public static long getTIMEOUT() {
		return TIMEOUT;
	}

	public static void setTIMEOUT(long tIMEOUT) {
		TIMEOUT = tIMEOUT;
	}

	public static int getBOOTSTRAP_NUM() {
		return BOOTSTRAP_NUM;
	}

	public static void setBOOTSTRAP_NUM(int bOOTSTRAP_NUM) {
		BOOTSTRAP_NUM = bOOTSTRAP_NUM;
	}

	public static BootstrapType getBOOTSTRAP_TYPE() {
		return BOOTSTRAP_TYPE;
	}

	public static void setBOOTSTRAP_TYPE(String arg) {
		BOOTSTRAP_TYPE = BootstrapType.valueOf(arg);
	}

	public static long getBOOTSTRAP_INTERVAL() {
		return BOOTSTRAP_INTERVAL;
	}

	public static void setBOOTSTRAP_INTERVAL(long bOOTSTRAP_INTERVAL) {
		BOOTSTRAP_INTERVAL = bOOTSTRAP_INTERVAL;
	}

	public static int getNUMBER_OF_SUPERPEERS() {
		return NUMBER_OF_SUPERPEERS;
	}

	public static void setNUMBER_OF_SUPERPEERS(int nUMBER_OF_SUPERPEERS) {
		NUMBER_OF_SUPERPEERS = nUMBER_OF_SUPERPEERS;
	}

	public static long getBLOCKING_INTERVAL() {
		return BLOCKING_INTERVAL;
	}

	public static void setBLOCKING_INTERVAL(long bLOCKING_INTERVAL) {
		BLOCKING_INTERVAL = bLOCKING_INTERVAL;
	}

	public static int getMAX_CONNECTION_CANDIDATES() {
		return MAX_CONNECTION_CANDIDATES;
	}

	public static void setMAX_CONNECTION_CANDIDATES(
			int mAX_CONNECTION_CANDIDATES) {
		MAX_CONNECTION_CANDIDATES = mAX_CONNECTION_CANDIDATES;
	}

	public static int getMAX_CONNECTION_TASTE_BUDDIES() {
		return MAX_CONNECTION_TASTE_BUDDIES;
	}

	public static void setMAX_CONNECTION_TASTE_BUDDIES(
			int mAX_CONNECTION_TASTE_BUDDIES) {
		MAX_CONNECTION_TASTE_BUDDIES = mAX_CONNECTION_TASTE_BUDDIES;
	}

	public static int getMAX_CONNECTION_RANDOM_PEERS() {
		return MAX_CONNECTION_RANDOM_PEERS;
	}

	public static void setMAX_CONNECTION_RANDOM_PEERS(
			int mAX_CONNECTION_RANDOM_PEERS) {
		MAX_CONNECTION_RANDOM_PEERS = mAX_CONNECTION_RANDOM_PEERS;
	}

	public static int getMAX_CONNECTION_UNCONNECTABLE_PEERS() {
		return MAX_CONNECTION_UNCONNECTABLE_PEERS;
	}

	public static void setMAX_CONNECTION_UNCONNECTABLE_PEERS(
			int mAX_CONNECTION_UNCONNECTABLE_PEERS) {
		MAX_CONNECTION_UNCONNECTABLE_PEERS = mAX_CONNECTION_UNCONNECTABLE_PEERS;
	}

	public static int getMESSAGE_TASTE_BUDDIES_NUMBER() {
		return MESSAGE_TASTE_BUDDIES_NUMBER;
	}

	public static void setMESSAGE_TASTE_BUDDIES_NUMBER(
			int mESSAGE_TASTE_BUDDIES_NUMBER) {
		MESSAGE_TASTE_BUDDIES_NUMBER = mESSAGE_TASTE_BUDDIES_NUMBER;
	}

	public static int getMESSAGE_RANDOM_PEERS_NUMBER() {
		return MESSAGE_RANDOM_PEERS_NUMBER;
	}

	public static void setMESSAGE_RANDOM_PEERS_NUMBER(
			int mESSAGE_RANDOM_PEERS_NUMBER) {
		MESSAGE_RANDOM_PEERS_NUMBER = mESSAGE_RANDOM_PEERS_NUMBER;
	}

	public static int getMESSAGE_PEER_PREFERENCES() {
		return MESSAGE_PEER_PREFERENCES;
	}

	public static void setMESSAGE_PEER_PREFERENCES(int mESSAGE_PEER_PREFERENCES) {
		MESSAGE_PEER_PREFERENCES = mESSAGE_PEER_PREFERENCES;
	}

	public static int getMAX_KNOWN_PREFERENCES_OF_PEERS() {
		return MAX_KNOWN_PREFERENCES_OF_PEERS;
	}

	public static void setMAX_KNOWN_PREFERENCES_OF_PEERS(
			int mAX_KNOWN_PREFERENCES_OF_PEERS) {
		MAX_KNOWN_PREFERENCES_OF_PEERS = mAX_KNOWN_PREFERENCES_OF_PEERS;
	}

	public static int getMAX_MY_PREFERENCES() {
		return MAX_MY_PREFERENCES;
	}

	public static void setMAX_MY_PREFERENCES(int mAX_MY_PREFERENCES) {
		MAX_MY_PREFERENCES = mAX_MY_PREFERENCES;
	}

	public static int getMAX_MEGACACHE() {
		return MAX_MEGACACHE;
	}

	public static void setMAX_MEGACACHE(int mAX_MEGACACHE) {
		MAX_MEGACACHE = mAX_MEGACACHE;
	}

	public static int getROUND_CHECK_CONNECTION() {
		return ROUND_CHECK_CONNECTION;
	}

	public static void setROUND_CHECK_CONNECTION(int rOUND_CHECK_CONNECTION) {
		ROUND_CHECK_CONNECTION = rOUND_CHECK_CONNECTION;
	}

	public static double getALPHA() {
		return ALPHA;
	}

	public static void setALPHA(double aLPHA) {
		ALPHA = aLPHA;
	}

	public static boolean isLOG_TO_FILE() {
		return LOG_TO_FILE;
	}

	public static void setLOG_TO_FILE(boolean lOG_TO_FILE) {
		LOG_TO_FILE = lOG_TO_FILE;
	}

	public static boolean isTRACE_TO_SCREEN() {
		return TRACE_TO_SCREEN;
	}

	public static void setTRACE_TO_SCREEN(boolean tRACE_TO_SCREEN) {
		TRACE_TO_SCREEN = tRACE_TO_SCREEN;
	}

	public static String getTRACE_FILE() {
		return TRACE_FILE;
	}

	public static void setTRACE_FILE(String tRACE_FILE) {
		TRACE_FILE = tRACE_FILE;
	}

	public static String getOUTPUT_LOG_FOLDER() {
		return OUTPUT_LOG_FOLDER;
	}

	public static void setOUTPUT_LOG_FOLDER(String oUTPUT_LOG_FOLDER) {
		OUTPUT_LOG_FOLDER = oUTPUT_LOG_FOLDER;
	}

	public static String getOUTPUT_GEPHY_FOLDER() {
		return OUTPUT_GEPHY_FOLDER;
	}

	public static void setOUTPUT_GEPHY_FOLDER(String oUTPUT_GEPHY_FOLDER) {
		OUTPUT_GEPHY_FOLDER = oUTPUT_GEPHY_FOLDER;
	}


	public static long getFILL_UP_NETWORK_TIME() {
		return FILL_UP_NETWORK_TIME;
	}

	public static void setFILL_UP_NETWORK_TIME(long fILL_UP_NETWORK_TIME) {
		FILL_UP_NETWORK_TIME = fILL_UP_NETWORK_TIME;
	}



	public static long getBUDDYCAST_INTERVAL() {
		return BUDDYCAST_INTERVAL;
	}

	public static void setBUDDYCAST_INTERVAL(long bUDDYCAST_INTERVAL) {
		BUDDYCAST_INTERVAL = bUDDYCAST_INTERVAL;
	}

	public static boolean isWATCH_FIREWALL() {
		return WATCH_FIREWALL;
	}

	public static void setWATCH_FIREWALL(boolean wATCH_FIREWALL) {
		WATCH_FIREWALL = wATCH_FIREWALL;
	}

	public static int getSEARCH_CIRCULAR() {
		return SEARCH_CIRCULAR;
	}

	public static void setSEARCH_CIRCULAR(int sEARCH_CIRCULAR) {
		SEARCH_CIRCULAR = sEARCH_CIRCULAR;
	}

	public static void setBOOTSTRAP_TYPE(BootstrapType bOOTSTRAP_TYPE) {
		BOOTSTRAP_TYPE = bOOTSTRAP_TYPE;
	}

	public static boolean isOUTPUT_GEPHY() {
		return OUTPUT_GEPHY;
	}

	public static void setOUTPUT_GEPHY(boolean oUTPUT_GEPHY) {
		OUTPUT_GEPHY = oUTPUT_GEPHY;
	}

	public static boolean isOUTPUT_DB() {
		return OUTPUT_DB;
	}

	public static void setOUTPUT_DB(boolean oUTPUT_DB) {
		OUTPUT_DB = oUTPUT_DB;
	}

	public static String getOUTPUT_DB_FOLDER() {
		return OUTPUT_DB_FOLDER;
	}

	public static void setOUTPUT_DB_FOLDER(String oUTPUT_DB_FOLDER) {
		OUTPUT_DB_FOLDER = oUTPUT_DB_FOLDER;
	}

	public static long getLOCAL_REPORT_ROUND() {
		return LOCAL_REPORT_ROUND;
	}

	public static void setLOCAL_REPORT_ROUND(long lOCAL_REPORT_ROUND) {
		LOCAL_REPORT_ROUND = lOCAL_REPORT_ROUND;
	}

	public static long getGLOBAL_REPORT_ROUND() {
		return GLOBAL_REPORT_ROUND;
	}

	public static void setGLOBAL_REPORT_ROUND(long gLOBAL_REPORT_ROUND) {
		GLOBAL_REPORT_ROUND = gLOBAL_REPORT_ROUND;
	}

	public static boolean isALWAYS_ONLINE() {
		return ALWAYS_ONLINE;
	}

	public static void setALWAYS_ONLINE(boolean aLWAYS_ONLINE) {
		ALWAYS_ONLINE = aLWAYS_ONLINE;
	}

	
}
