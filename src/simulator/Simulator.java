package simulator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Vector;

import output.gephi.GephiBuilder;
import output.sqlite.SqlLiteDbGenerator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import debug.Debug;
import protocols.BuddyCast;
import similarityModels.CooccurrenceSimilarity;
import similarityModels.RandomSimilarity;
import similarityModels.SimilarityProtocol;
import entities.Peer;
import log.LogReader;
import log.entities.LogPeer;
import eduni.simjava.Sim_system;


public class Simulator {
		
	public static void main(String[] args) {
		
		
		Debug.system(System.currentTimeMillis()/1000, "Initialize Simulation...", System.out);
		
		// generate file name for output
		String fileName = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date());
		
		
		/*
		 * Read Configuration Data from file
		 */
		
		// read console specified config file, if it doesn't exist, we set default filename
		String configFile;
		if(args.length != 0 && !args[0].isEmpty()) {
			configFile = args[0];
		} else
			configFile = "config.json";
		
		
		try {
			Debug.system(System.currentTimeMillis()/1000, "Read configuration...", System.out);
			BufferedReader br = new BufferedReader(new FileReader(configFile));	
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.excludeFieldsWithModifiers(java.lang.reflect.Modifier.STATIC);
			gsonBuilder.excludeFieldsWithModifiers(java.lang.reflect.Modifier.TRANSIENT);

			Gson gson = gsonBuilder.create();
			gson.fromJson(br, Settings.class);
			Debug.system(System.currentTimeMillis()/1000, "Successful reading...", System.out);
		} catch (FileNotFoundException e) {
			Debug.system(System.currentTimeMillis()/1000, "Configuration file read is failed!", System.err);
			Debug.system(System.currentTimeMillis()/1000, "Error: " + e.getLocalizedMessage(), System.err);
			Debug.system(System.currentTimeMillis()/1000, "Continue with default settings...", System.err);
		}  		
		
		
		/*
		 * Initialize Output
		 */
		if(Settings.OUTPUT_DB) SqlLiteDbGenerator.boilerDb(fileName);
		if(Settings.OUTPUT_GEPHY) GephiBuilder.boilerGephiFile(fileName);
		
		
		/*
		 * Set up similarity protocol
		 */
		SimilarityProtocol similarity;		
		
		if(Settings.SIMILARITY_TYPE.equals("RANDOM"))
			similarity = new RandomSimilarity();
		else if(Settings.SIMILARITY_TYPE.equals("COOCCURRENCE"))
			similarity = new CooccurrenceSimilarity();
		else
			similarity = new RandomSimilarity();
			
		
		/*
		 * Simulation System Initialize
		 */
                
		Debug.system(System.currentTimeMillis()/1000, "Simulation is starting...", System.out);
		Sim_system.initialise();
        Sim_system.set_trace_detail(false, false, false);		

        LogReader reader = new LogReader(Container.fileName);
		Vector<LogPeer> peers = reader.readAllUsersData();
		Collections.sort(peers);
		int i=0;
		
		
		// Peer reading
		for(LogPeer lp : peers)  {

				
			Peer s = new Peer(Integer.toString(lp.getUserId()-1), 
								lp, 
								(i++<Settings.NUMBER_OF_SUPERPEERS)?true:false, 
										new BuddyCast(), similarity);
			Sim_system.add(s);
			Container.allPeers.add(s);

		}
		
		Container.superPeers = Container.allPeers.subList(0, Settings.NUMBER_OF_SUPERPEERS);		
		
		for(Peer p : Container.superPeers)
			p.getProtocol().firstBootstrap();
		

		Sim_system.run();

		
		// End Output
		Debug.system(System.currentTimeMillis()/1000, "Simulation end!", System.out);
	
		
		
				
		/*
		 *	Write output files
		 */
		try {
			
			if(Settings.OUTPUT_DB && SqlLiteDbGenerator.db.isInTransaction())
				SqlLiteDbGenerator.commit();
			
			
			if(Settings.OUTPUT_GEPHY) GephiBuilder.saveGexf();
			
			if(Settings.LOG_TO_FILE) Debug.writeToFile(fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	


}
