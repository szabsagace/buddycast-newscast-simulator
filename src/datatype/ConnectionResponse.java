package datatype;

public enum ConnectionResponse {
	OK,
	OFFLINE,
	NOT_CONNECTABLE,
	IN_BLOCKLIST,
	IN_CONNECTED_LIST,
	NOT_CONNECTABLE_MYSELF,
	DISCONNECTED
}
