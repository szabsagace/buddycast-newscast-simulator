package datatype;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class LimitedQueue<T> extends ConcurrentLinkedQueue<T>
	implements Queue<T>, Iterable<T>  {

    /**
	 * 
	 */
	private static final long serialVersionUID = -757705745187823180L;
	private final int limit;
    //private final ConcurrentLinkedQueue<T> this = new ConcurrentLinkedQueue<T>();

    public LimitedQueue(int limit) {
        this.limit = limit;
    }

    private boolean trim() {
    	if(limit == -1) return false;
    	
        boolean changed = this.size() > limit;
        while (this.size() > limit) {
            this.remove();
        }
        return changed;
    }
    
    public String toString() {
    	return super.toString();
    }

    public boolean add(T o) {
    	
        boolean trimmed = trim();
        return super.add(o) || trimmed;
    }

    
    public int size() {
        return super.size();
    }

    
    public boolean isEmpty() {
        return super.isEmpty();
    }

    
    public boolean contains(Object o) {
        return super.contains(o);
    }

    
    public Iterator<T> iterator() {
        return super.iterator();
    }

    
    public Object[] toArray() {
        return super.toArray();
    }

    
    @SuppressWarnings("hiding")
	public <T> T[] toArray(T[] a) {
        return super.toArray(a);
    }

    
    public boolean remove(Object o) {
        return super.remove(o);
    }

    
    public boolean containsAll(Collection<?> c) {
        return super.containsAll(c);
    }

    
    public boolean addAll(Collection<? extends T> c) {
        boolean trimmed = trim();
        return super.addAll(c) || trimmed;
    }

    
    public boolean removeAll(Collection<?> c) {
        return super.removeAll(c);
    }

    
    public boolean retainAll(Collection<?> c) {
        return super.retainAll(c);
    }

    
    public void clear() {
        super.clear();
    }

    
    public boolean offer(T e) {
        boolean changed = super.offer(e);
        boolean trimmed = trim();
        return changed || trimmed;
    }

    
    public T remove() {
        return super.remove();
    }

    
    public T poll() {
        return super.poll();
    }

    
    public T element() {
        return super.element();
    }

    
    public T peek() {
        return super.peek();
    }
}