package datatype;


import java.util.Map;


public class CacheMap<K, V>  extends ConcurrentLinkedHashMap<K, V> {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int maxSize;
	public CacheMap(int maxSize) {
		this.maxSize = maxSize;
	}
	
	public CacheMap(int maxSize, Comparable<CacheMap<K,V>> comparator) {
		this.maxSize = maxSize;
	}
	

	
	@Override
	protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
		return this.size() > this.maxSize;
	}	
	
	
	

}
