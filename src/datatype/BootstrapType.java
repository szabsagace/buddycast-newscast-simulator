package datatype;

public enum BootstrapType {
	ONLY_ONCE,
	WHEN_CONNCANDIDATE_EMPTY,
	BOOTSTRAP_INTERVAL
}
