package comparator;

import java.util.Comparator;
import java.util.Map;

import datatype.Pair;

public class RecentPeerComparator implements Comparator<Map.Entry<Integer, Pair<Long, Integer>>> {


	public int compare(Map.Entry<Integer, Pair<Long, Integer>> first, Map.Entry<Integer, Pair<Long, Integer>> second) {
		if(first.getValue().getFirst() > second.getValue().getFirst())
			return -1;
		else if(first.getValue().getFirst() < second.getValue().getFirst())
			return 1;
		else
			return 0;
	}

}
