package comparator;

import java.util.Comparator;
import java.util.Map;
import java.util.Random;

import datatype.Pair;

public class SimilarityConnectionComparator implements Comparator<Map.Entry<Integer, Pair<Long,Integer>>> {

	private int seedGenerator = 0;
	
	public int compare(Map.Entry<Integer, Pair<Long,Integer>> first, Map.Entry<Integer, Pair<Long,Integer>> second) {
		Random generator = new Random();
		generator.setSeed(System.currentTimeMillis()*seedGenerator++);
		int randomNumberForFirst = generator.nextInt();
		generator.setSeed(System.currentTimeMillis()*seedGenerator++);		
		int randomNumberForSecond = generator.nextInt();
	
		
		// similarity
		if(first.getValue().getFirst() > second.getValue().getFirst())
			return -1;
		else if(first.getValue().getFirst() < second.getValue().getFirst())
			return 1;
		// connection time check
		else if(first.getValue().getSecond() > second.getValue().getSecond())
			return -1;
		else if(first.getValue().getSecond() < second.getValue().getSecond())
			return 1;
		// random number
		else if(randomNumberForFirst > randomNumberForSecond)
			return -1;
		else
			return 1;
		
	}

}