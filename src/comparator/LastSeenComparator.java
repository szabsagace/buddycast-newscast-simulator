package comparator;

import java.util.Comparator;
import java.util.Map;
import java.util.Random;

public class LastSeenComparator implements Comparator<Map.Entry<Integer, Long>> {
 
	private int genNum = 0;
	
	public int compare(Map.Entry<Integer, Long> first, Map.Entry<Integer, Long> second) {
		Random generator = new Random();
		generator.setSeed(System.currentTimeMillis()*genNum++);
		int randomNumberForFirst = generator.nextInt();
		generator.setSeed(System.currentTimeMillis()*genNum++);

		int randomNumberForSecond = generator.nextInt();
	
		if(first.getValue() > second.getValue())
			return -1;
		else if(first.getValue() < second.getValue())
			return 1;
		else if(randomNumberForFirst > randomNumberForSecond)
			return -1;
		else
			return 1;
	}

}