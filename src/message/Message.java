package message;


public abstract class Message {
	
	/**
	 * Reply
	 */
	public boolean reply;
	
	public int senderId;
	
	public boolean isConnectable;
}
