package message;

import java.util.Map;

import datatype.LimitedQueue;
import datatype.Pair;

public class BuddycastMessage extends Message {

	/**
	 * Sender's preferences
	 */
	public LimitedQueue<Integer> myPreferences;
	
	/**
	 * TasteBuddies
	 */
	public Map<Integer, Pair<LimitedQueue<Integer>, Long>> tasteBuddies;
	
	
	/**
	 * RandomPeers
	 */
	public Map<Integer, Long> randomPeers;
	

	
	
}
