package log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Vector;

import log.entities.Event;
import log.entities.LogPeer;
import log.entities.Status;

public class LogReader {
	
	private String logFile;
	
	public LogReader(String logFile) {
		this.logFile = logFile;
	}
	
	public Vector<LogPeer> readAllUsersData() {
		Vector<String> lines = this.readLines();
		Vector<LogPeer> peers = new Vector<LogPeer>();
		
		for(String item : lines) {
			String[] splittedItems = item.split(" ");
			Vector<Event> events = new Vector<Event>();
			
			LogPeer peer = new LogPeer(Integer.parseInt(splittedItems[0]));
			
			for(int i=1; i<splittedItems.length; i++) {
				String[] eventItems = splittedItems[i].split("\\.");
				
				Event event = new Event(Long.parseLong(eventItems[0]));
								
				if(eventItems[1].equals("U")) {
					event.setEventStatus(Status.ONLINE);
					event.setFirewall(eventItems[2].equals("1"));
				}
				else if(eventItems[1].equals("+")) {
					event.setEventStatus(Status.SEARCH);
					event.setSearchedItem(Integer.parseInt(eventItems[2]));
				}
				else
					event.setEventStatus(Status.OFFLINE);
			
				events.add(event);
			}
			
			Collections.sort(events);
			peer.setEvents(events);
			peers.add(peer);
		}
		
		return peers;
	}
	
	private Vector<String> readLines() {
		Vector<String> lines = new Vector<String>();
		
		try{
			   FileInputStream fstream = new FileInputStream(this.logFile);
			   @SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			   String strLine;

			   while ((strLine = br.readLine()) != null)   {
			     lines.add(strLine);
			   }
			} catch (Exception e) {
			     System.err.println("Error: " + e.getMessage());
			} 
		return lines;
	}
	
	
}
