package log.entities;

import java.util.Collections;
import java.util.Vector;

public class LogPeer implements Comparable<LogPeer>{
	private int userId;
	private Vector<Event> events;
	
	
	public LogPeer(int userId) {
		this.userId = userId;
	}
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Vector<Event> getEvents() {
		return events;
	}
	
	public void setEvents(Vector<Event> events) {
		this.events = events;
		Collections.sort(this.events);
	}
	
	public long getNearestEvent(Long timeStamp, Status statusSearch) {
		for(Event e : events) {
			if(e.getTimeStamp() > timeStamp && statusSearch == e.getEventStatus()) 
				return e.getTimeStamp(); 
			
		}
		return 0;
	}
	
	public String toString() {
		String out = "---------------------\n";
		out += "USER:" + this.userId + "\n";
		out += "---------------------\n";
		for(Event e : this.events) {
			out += e.toString() + "\n";			
		}
		out += "------------------------\n";
		return out;
	}

	public int compareTo(LogPeer arg0) {
		if(this.userId < arg0.getUserId())
			return -1;
		else
			return 1;
	}
	
	
	
	
}
