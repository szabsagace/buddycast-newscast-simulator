package log.entities;

public enum Status {
	ONLINE, OFFLINE, SEARCH, PROTOCOL_CALL
}
