package log.entities;

import simulator.Container;

public class Event implements Comparable<Event> {
	private long timeStamp;
	private Status eventStatus;
	private int searchedItem;
	private boolean isFirewall;
	
	public Event(long timeStamp) {
		this.timeStamp = timeStamp;
		
		if(timeStamp < Container.SIM_BEGIN)
			Container.SIM_BEGIN = timeStamp;
		
		if(timeStamp > Container.SIM_END)
			Container.SIM_END = timeStamp;
	}
	
	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(int timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Status getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(Status eventStatus) {
		this.eventStatus = eventStatus;
	}


	public int getSearchedItem() {
		return searchedItem;
	}


	public void setSearchedItem(int searchedItem) {
		this.searchedItem = searchedItem;
	}


	public boolean isFirewall() {
		return isFirewall;
	}


	public void setFirewall(boolean isFirewall) {
		this.isFirewall = isFirewall;
	}

	public int compareTo(Event arg0) {
		if(this.timeStamp < arg0.getTimeStamp())
			return -1;
		else if(this.timeStamp == arg0.getTimeStamp())
			return 0;
		else
			return 1;
	}
	
	public String toString() {
		return "[" + this.getTimeStamp() + "]" + this.getEventStatus();
	}
}
