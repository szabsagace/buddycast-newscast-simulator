package similarityModels;

import java.util.Random;

import datatype.CacheMap;
import datatype.LimitedQueue;

public class RandomSimilarity implements SimilarityProtocol {

	private int seedHelper = 0;
	
	public int getSimilarity(int peerId, LimitedQueue<Integer> myPreferences, CacheMap<Integer, LimitedQueue<Integer>> knownPreferences) {
		Random rand = new Random();
		rand.setSeed(System.currentTimeMillis()*seedHelper++);
		return rand.nextInt(100);
	}

}
