package similarityModels;

import datatype.CacheMap;
import datatype.LimitedQueue;

public class CooccurrenceSimilarity implements SimilarityProtocol {

	public int getSimilarity(int peerId, LimitedQueue<Integer> myPreferences,
			CacheMap<Integer, LimitedQueue<Integer>> knownPreferences) {
		synchronized(knownPreferences.get(peerId)) {
			int cooccurrence = 0;
	
			if(knownPreferences.get(peerId) == null)
				return 0;
			
			for (Integer e : knownPreferences.get(peerId)) {
				if (myPreferences.contains(e))
					cooccurrence++;
	
			}
	
			return new Double((1000 * cooccurrence)/ 
					Math.sqrt(myPreferences.size() * 
							knownPreferences.get(peerId).size())).intValue();
		}
	}

}
