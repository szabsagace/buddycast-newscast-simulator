package similarityModels;

import datatype.CacheMap;
import datatype.LimitedQueue;

public interface SimilarityProtocol {
	public int getSimilarity(int peerId, LimitedQueue<Integer> myPreferences, CacheMap<Integer, LimitedQueue<Integer>> knownPreferences);
}
