package debug;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Timestamp;

import simulator.Settings;

public class Debug {

	private static StringBuffer log;

	
	static {
		log = new StringBuffer();
	}
	
	private static void logInformation(String message) {
		synchronized(log) {
			log.append(message);
			log.append(System.getProperty("line.separator"));
		}
	}


	
	
	public static void debug(long time, DebugTypes type, String message) {
		long t = time * 1000l;
		String mess = "[" + new Timestamp(t).toString() + "] [" + type.name() + "] " + message;
		
		if(Settings.LOG_TO_FILE)
			logInformation(mess);
		
		if(Settings.TRACE_TO_SCREEN) {
				System.out.flush();
				System.out.println(mess);
	

		}
	}
	
	
	public static void system(long time, String message, PrintStream out) {
		String mess = "[" + new Timestamp(time*1000l).toString() + "] " + message;

		out.println(mess);
	}
	
	 public static void writeToFile(String fileName) throws IOException {  
		 	File folderTest = new File(Settings.OUTPUT_LOG_FOLDER);
		 	if(!folderTest.exists())
		 		folderTest.mkdir();
		 
	        BufferedWriter out = new BufferedWriter(new FileWriter(Settings.OUTPUT_LOG_FOLDER + File.separator + fileName + "-trace.txt"));  
	        out.write(log.toString());  
	        out.flush();  
	        out.close();  
	 } 
	
}
