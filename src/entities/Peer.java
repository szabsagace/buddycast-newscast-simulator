package entities;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

import output.Statistic;
import output.gephi.GephiBuilder;
import output.sqlite.SqlLiteDbGenerator;
import datatype.ConnectionResponse;
import debug.Debug;
import debug.DebugTypes;
import log.entities.Event;
import log.entities.LogPeer;
import log.entities.Status;
import message.Message;
import protocols.BuddyCast;
import protocols.Protocol;
import similarityModels.SimilarityProtocol;
import simulator.Container;
import simulator.Settings;
import eduni.simjava.Sim_entity;
import eduni.simjava.Sim_event;
import eduni.simjava.Sim_system;

/**
 * 
 * @author Szabolcs
 *
 */
public class Peer extends Sim_entity {
	
	/*---------------------------------------------------
	   Peer Profile
	 ----------------------------------------------------*/
	private int peerId;
	private LogPeer peerProfile;
	private boolean superPeer;
	private boolean isConnectable;
	private boolean isOnline;
	private Protocol protocol;
	private SimilarityProtocol similarity;
	private Statistic stat;
	private boolean firstConnect;
	public boolean firstOnline;

	private static int globalReport = 0;
	/*---------------------------------------------------
	   Peer Local Database
	 ----------------------------------------------------*/
	private Map<Integer, Long> connections;
	{
		connections = new ConcurrentHashMap<Integer, Long>();
	}
	
	
	/*---------------------------------------------------
	   Constructors
	 ----------------------------------------------------*/
	public Peer(final String userId) {
		super(userId);
		this.peerId = Integer.parseInt(userId);
	}
	
	public Peer(final String userId, final LogPeer peerProfile, 
			final boolean superPeer, final Protocol protocol, final
			SimilarityProtocol similarity) {
		super(userId);

		this.peerId = Integer.parseInt(userId);
		this.peerProfile = peerProfile;
		this.superPeer = superPeer;
		this.isOnline = false;
		this.protocol = protocol;
		this.protocol.setHostPeer(this);
		this.similarity = similarity;
		this.stat = new Statistic();
		this.firstConnect = true;
		this.firstOnline = true;
	}
	
	/*---------------------------------------------------
	   Simulation Body
	 ----------------------------------------------------*/
	public final void body() {
		this.cronBuddyCastCalls();
		for(Event e : peerProfile.getEvents()) {
			Container.events_num++;
			
			if(Settings.ALWAYS_ONLINE) {
				if(e.getEventStatus() == Status.SEARCH)
					sim_schedule(this.peerId, e.getTimeStamp(), e.getEventStatus().ordinal(), e.getSearchedItem());
				else if(e.getEventStatus() == Status.PROTOCOL_CALL) 
					sim_schedule(this.peerId, e.getTimeStamp(), e.getEventStatus().ordinal());
				
				
				
				if(this.firstOnline && e.getEventStatus() == Status.ONLINE) {
					sim_schedule(this.peerId, Container.SIM_BEGIN, e.getEventStatus().ordinal(), Settings.WATCH_FIREWALL ? e.isFirewall() : true);								
					this.firstOnline = false;
				}
				continue;
			} 		

			   if(e.getEventStatus() == Status.SEARCH)
					sim_schedule(this.peerId, e.getTimeStamp(), e.getEventStatus().ordinal(), e.getSearchedItem());
				else if(e.getEventStatus() == Status.ONLINE)
					sim_schedule(this.peerId, e.getTimeStamp(), e.getEventStatus().ordinal(), Settings.WATCH_FIREWALL ? e.isFirewall() : true);			
				else
					sim_schedule(this.peerId, e.getTimeStamp(), e.getEventStatus().ordinal());
			
		}


		
		while(Sim_system.running()) {
			Sim_event event = new Sim_event();
	        sim_get_next(event);
	        System.out.flush();
	        
	        
	        switch(event.get_tag()) {
	        	// ONLINE
	        	case 0:
	        		if(this.firstConnect) {
		        		Debug.debug(getTime(), DebugTypes.INFO, getPeerId() + ": FIRST BOOTSTRAP");
		        		this.protocol.firstBootstrap();	        			
	        			this.firstConnect = false;
	        		}
	        		
	        		this.isConnectable = (Boolean) event.get_data();
	        		Debug.debug(getTime(), DebugTypes.INFO, getPeerId() + ": ONLINE (Connectable: " + this.isConnectable + ")");
	        		this.online();
					
					if(Settings.OUTPUT_GEPHY) GephiBuilder.addNode(this.peerId, this.getTime());
	        		break;
	        		
	        	// OFFLINE
	        	case 1: 
	        		Debug.debug(getTime(), DebugTypes.INFO, getPeerId() + ": OFFLINE");

	        		this.offline();
					if(Settings.OUTPUT_GEPHY) GephiBuilder.clearNode(this.peerId, this.getTime());

	        		break;
	        	
	        	// SEARCH
	        	case 2:
	        		// Fill up network
	        		if(this.getTime() <= Container.SIM_BEGIN + Settings.FILL_UP_NETWORK_TIME) 
	        			this.protocol.searchAction((Integer)event.get_data(), true);
	        		else
	        			this.protocol.searchAction((Integer)event.get_data(), false);
	        		
	        		break;
	        	
	        	// PROTOCOL ACTION
	        	case 3:
	        		if(this.isOnline)
	        			this.protocol.action();
	        		
	        		break;

				default :
					break;
	        }
	        
	        if(Settings.OUTPUT_DB && Peer.globalReport++ % Settings.GLOBAL_REPORT_ROUND == 0 )
	        	this.reportFromAllPeers();
	        
	        sim_completed(event);

		}
		
		Debug.debug(getTime(), DebugTypes.STAT, getStat().toString());

		
	}

	/*---------------------------------------------------
	   Peer methods
	 ----------------------------------------------------*/
	
	/**
	 * Peer is going to be online
	 */
	private synchronized void online() {
		this.isOnline = true;
		this.protocol.bootstrap(Settings.BOOTSTRAP_NUM); 
	}
	
	
	/**
	 * Peer is going to be offline
	 */
	private synchronized void offline() {
		this.isOnline = false;
		this.resetValues();
	}
	
	/**
	 * Close all connections and reset protocol values
	 */
	private synchronized void resetValues() {		
		for(Integer peerId : connections.keySet())
			this.closeConnection(peerId);

		this.protocol.resetValues();
	}
	
	/*---------------------------------------------------
	   Peer 'connections' variable administration method
	 ----------------------------------------------------*/
	
	/**
	 * Add new connection to connections map.
	 * @param peerId the peer who adding connections map
	 */
	private void addConnection(final int peerId) {
		connections.put(peerId, this.getTime() + Settings.TIMEOUT);
		this.stat.openedConnections++;
			
		this.protocol.addedConnection(peerId);
	}
	
	/**
	 * Close connection of peer
	 * @param peerId the peer id, who wants to close
	 */
	public void closeConnection(final int peerId) {
		connections.remove(peerId);
	
		this.stat.closedConnections++;
		if(Settings.OUTPUT_GEPHY) GephiBuilder.clearEdge(this.getPeerId(), peerId, this.getTime());

		if(Container.allPeers.get(peerId).getConnections().containsKey(this.peerId)) 
			Container.allPeers.get(peerId).closeConnection(this.peerId);			
	}
	
	/**
	 * Get all connections
	 */
	public final Map<Integer, Long> getConnections() {
		return this.connections;
	}
	
	/**
	 * Check that peer is connected, or not.
	 * @param peerId the peer who wants to check
	 */
	public boolean isConnected(final int peerId) {
		return this.connections.containsKey(peerId);
	}
	
	/**
	 * Check all connections, and if expired, we closed a connections.
	 * @param keepAlive
	 */
	public synchronized void checkConnections(boolean keepAlive) {
		Iterator<Entry<Integer, Long>> it = this.connections.entrySet().iterator();
		
		long now = this.getTime();
		
		while(it.hasNext()) {
			Entry<Integer,Long> entry = (Entry<Integer, Long>) it.next();
			
			if(entry.getValue() <= now)
				this.closeConnection(entry.getKey());
			else if(keepAlive && this.protocol.checkKeepAlive(entry.getKey()))
				this.sendKeepAliveMessage(entry.getKey());
		}
	}
	
	/**
	 * Send a keep alive message to another peer
	 * @param targetPeer peer id
	 */
	public void sendKeepAliveMessage(int targetPeer) {
		if(this.isConnected(targetPeer) 
		&& Container.allPeers.get(targetPeer).gotKeepAliveMessage(this.peerId) == ConnectionResponse.OK)
				this.connections.put(targetPeer, this.getTime() + Settings.TIMEOUT);
		
	}
	
	/**
	 * Got a keep alive message
	 * @param sender Sender id
	 */
	public ConnectionResponse gotKeepAliveMessage(int sender) {
		if(this.isConnected(sender)) {
			this.connections.put(sender, this.getTime() + Settings.TIMEOUT);
			return ConnectionResponse.OK;
		} else 
			return ConnectionResponse.DISCONNECTED;
	}
	
	/*---------------------------------------------------
	   Peer connection simulation methods
	 ----------------------------------------------------*/
	
	/**
	 * Connect to another peer
	 * @param target The peer's id, who we wants to connect
	 * @return response
	 */
	public synchronized final ConnectionResponse connectPeer(final int target) {
		ConnectionResponse response = Container.allPeers.get(target).acceptConnection(this.peerId);
		if(response == ConnectionResponse.OK) {
			this.addConnection(Container.allPeers.get(target).getPeerId());
			//sim_trace(Sim, this.peerId + " connected to " + target);
		}	
		return response;
	}
	
	/**
	 * 
	 * @param sender
	 * @return
	 */
	public synchronized final ConnectionResponse acceptConnection(final int sender) {
		ConnectionResponse response = this.responsePredit(sender, true); // ide meg kellllllll
		
		if(response == ConnectionResponse.OK) {
			this.addConnection(Container.allPeers.get(sender).getPeerId());
		}
		
		return response;
	}
	
	/**
	 * 
	 * @param sender
	 * @param reply
	 * @return
	 */
	private final ConnectionResponse responsePredit(final int sender, final boolean reply) {
		
		if(this.getConnections().containsKey(sender)) {
			return ConnectionResponse.IN_CONNECTED_LIST;
		} 
		
		if(sender == this.peerId) {
			return ConnectionResponse.NOT_CONNECTABLE_MYSELF;
		}
		
		if(!this.isOnline) {
			return ConnectionResponse.OFFLINE;
		}
		
		if(reply && !this.isConnectable) {
			return ConnectionResponse.NOT_CONNECTABLE;
		}
		
		if(this.protocol.isBlocked(sender, ((BuddyCast)this.protocol).getReceiversBlockList())) 
			return ConnectionResponse.IN_BLOCKLIST;
				
		return ConnectionResponse.OK;	
	}
	
	/*
	 * Message sending
	 * 
	 */
	/**
	 * Message Sending
	 * @param recipientId
	 * @param message
	 * @param reply
	 */
	public void sendMessage(int recipientId, Message message) {
		Container.allPeers.get(recipientId).gotMessage(message);
		this.stat.sentMessages++;
	}
	
	/**
	 * Got Message
	 * @param sender
	 * @param message
	 * @param reply
	 */
	public void gotMessage(Message message) {
		this.protocol.handleMessage(message);
		this.stat.receivedMessages++;
	}
	

	/*---------------------------------------------------
	   Peer Getters/Setters
	 ----------------------------------------------------*/
	public final int getPeerId() {
		return this.peerId;
	}
	
	public long getTime() {
		return new Double(Sim_system.clock()).longValue();
	}
	
	public boolean isSuperPeer() {
		return this.superPeer;
	}
	
	public boolean isConnectable() {
		return this.isConnectable;
	}
	
	public Protocol getProtocol() {
		return this.protocol;
	}
	
	public SimilarityProtocol getSimilarity() {
		return this.similarity;
	}
	
	public Statistic getStat() {
		return this.stat;
	}
	

	
	/**
	 * Cron Buddycast Calls
	 */
	private void cronBuddyCastCalls() {
		Vector<Event> events = this.peerProfile.getEvents();
		
		long timeStamp = Container.SIM_BEGIN;
		while(timeStamp < Container.SIM_END) {
			// Ha online vagyok
			long tmpTimeStamp = timeStamp + Settings.BUDDYCAST_INTERVAL,
				 nearestOnline = this.peerProfile.getNearestEvent(tmpTimeStamp, Status.ONLINE);
			
			if(!isOnline(tmpTimeStamp)) {
				tmpTimeStamp = nearestOnline;
				if(tmpTimeStamp == 0) 
					break;
			}
			
			Event e = new Event(tmpTimeStamp);
			e.setEventStatus(Status.PROTOCOL_CALL);
			events.add(e);
			timeStamp = tmpTimeStamp;
		}
		this.peerProfile.setEvents(events);
	}
	
	/**
	 * Is Online method for cron Buddycast Calls method
	 * @param timeStamp
	 * @return
	 */
	private boolean isOnline(long timeStamp) {
		
		if(Settings.ALWAYS_ONLINE)
			return true;
		
		long onlineTime = this.peerProfile.getNearestEvent(timeStamp, Status.ONLINE);
		long offlineTime = this.peerProfile.getNearestEvent(timeStamp, Status.OFFLINE);
		
		// Felt�telezz�k, hogy adott id�pontban, ha az onlineba l�p�s
		// ideje k�zelebb van, mint az offline, akkor amikor megk�rdezt�k, akkor
		// tuti offline-ok voltunk. De ez csak akkor �rv�nyes, hogy ha ilyen nem lehet
		// U-U vagy D-D action.
		if(offlineTime >= onlineTime)
			return false;
		else
			return true;
		
	}
	
	
	
	/*
	 * Global Report 
	 */
	
	private void reportFromAllPeers() {
		Debug.debug(getTime(), DebugTypes.STAT, "REPORT CALLED FROM ALL PEERS");
		

		SummaryStatistics sendBlockList = new SummaryStatistics();
		SummaryStatistics receivedBlockList = new SummaryStatistics();
		SummaryStatistics connectedTasteBuddiesNum = new SummaryStatistics();
		SummaryStatistics connectedRandomPeersNum = new SummaryStatistics();
		SummaryStatistics similarityTasteBuddies = new SummaryStatistics();
		SummaryStatistics similarityRandomPeers = new SummaryStatistics();
		SummaryStatistics buddycastSentAverage = new SummaryStatistics();

		SummaryStatistics startedSearch = new SummaryStatistics();
		SummaryStatistics hittedSearch = new SummaryStatistics();
		SummaryStatistics newTorrentsInjected = new SummaryStatistics();

		
		int isnotconnectableonline = 0;
		int isconnectableonline = 0;
		int offline = 0;
		
		for(Peer p : Container.allPeers) {	
			if(p.isOnline) {
				p.protocol.updateStatisticData();
				sendBlockList.addValue(p.getStat().sendBlockListNum);
				receivedBlockList.addValue(p.getStat().receivedBlockListNum);
				connectedTasteBuddiesNum.addValue(p.getStat().connectedTasteBuddiesNum);
				connectedRandomPeersNum.addValue(p.getStat().connectedRandomPeersNum);
				similarityTasteBuddies.addValue(p.getStat().averageSimilarityTasteBuddies);
				similarityRandomPeers.addValue(p.getStat().averageSimilarityRandomPeers);
				
				if(p.isConnectable) isconnectableonline++; else isnotconnectableonline++;
			} else
				offline++;

			startedSearch.addValue(p.getStat().startedSearch);
			hittedSearch.addValue(p.getStat().hittedSearch);
			newTorrentsInjected.addValue(p.getStat().newTorrentImported);
			
			buddycastSentAverage.addValue(p.getStat().sentMessages);
			
		}
		SqlLiteDbGenerator.addItem("global_report", this.getTime(), sendBlockList.getMean(), receivedBlockList.getMean()
				, similarityTasteBuddies.getMean(), similarityRandomPeers.getMean(), connectedTasteBuddiesNum.getMean(),
				connectedRandomPeersNum.getMean(), buddycastSentAverage.getMean(),
				isconnectableonline, isnotconnectableonline, offline, 
				new Double(startedSearch.getSum()).intValue(),new Double(hittedSearch.getSum()).intValue(),
				new Double(newTorrentsInjected.getSum()).intValue());
	}

	
	
	
}
